package com.ansome22.moviesandtvshows.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.ansome22.moviesandtvshows.MySingleton;
import com.ansome22.moviesandtvshows.R;
import com.ansome22.moviesandtvshows.model.ListItem;

import java.util.List;

/**
 * Created by ansome22 on 6/05/2017.
 */

public class DerpAdapter extends RecyclerView.Adapter<DerpAdapter.DerpHolder> {
    private List<ListItem> listData;
    private LayoutInflater inflater;

    private ItemClickCallback itemClickCallback;

    public interface ItemClickCallback{
        void onitemClick(int p);
    }

    public void setItemClickCallback(final  ItemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }

    public DerpAdapter(List<ListItem> listData, Context c) {
        this.inflater = LayoutInflater.from(c);
        this.listData = listData;
    }


    @Override
    public DerpHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_item, parent,false);
        return new DerpHolder(view);
    }

    @Override
    public void onBindViewHolder(DerpHolder holder, int position) {

        ListItem item = listData.get(position);
        holder.title.setText(item.getTitle());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    class DerpHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private View container;


        public DerpHolder(View itemView) {
            super(itemView);
            //get id of each element
            title = (TextView) itemView.findViewById(R.id.item_text);
            container = itemView.findViewById(R.id.cont_item_root);
            container.setOnClickListener(this);
        }

        @Override
        //listen for user to clikc on item
        public void onClick(View v) {
            //find out what they did and didnt click on
            if(v.getId() == R.id.cont_item_root){
                itemClickCallback.onitemClick(getAdapterPosition());
            }
        }
    }



    public void remove(){
        for(int i=0; i<listData.size(); i++){
            listData.remove(i);
        }
    }

}
