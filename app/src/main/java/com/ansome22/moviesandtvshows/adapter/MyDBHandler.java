package com.ansome22.moviesandtvshows.adapter;

/**
 * Created by ansome22 on 8/05/2017.
 */

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;

import com.ansome22.moviesandtvshows.dummy.DummyContent;
import com.ansome22.moviesandtvshows.model.Favorites;

import java.util.ArrayList;
import java.util.List;

//class responsible for interaction with the database
public class MyDBHandler extends SQLiteOpenHelper{

    private  static final int DATABASE_VERSION = 1;
    private  static final String DATABASE_NAME = "favorites.db";
    public static final String TABLE_FAVORITES = "favorites";
    //colomns
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PRODUCTNAME = "productname";
    public static final String COLUMN_OVERVIEW = "overview";
    public static final String COLUMN_RELEASEDATE = "release_date";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_VOTEAVERAGE = "vote_average";

    //houseceeping
    //pass things into superclass
    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    //first time
    //needs to know what to do
    //so make a query
    @Override
    public void onCreate(SQLiteDatabase db) {
        //create table
        String query = "CREATE TABLE " + TABLE_FAVORITES + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PRODUCTNAME + " TEXT, " +
                COLUMN_OVERVIEW + " TEXT, " +
                COLUMN_RELEASEDATE + " TEXT, " +
                COLUMN_STATUS + " TEXT, " +
                COLUMN_VOTEAVERAGE + " TEXT " +
                ");";
        //execute sql
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //drops table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVORITES);

        //need to recreate with new structure
        onCreate(db);
    }

    //add new row to the table in database
    public void addProduct(Favorites favorite){
        //set different values for different columns and insert into one statement
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, favorite.get_id());
        values.put(COLUMN_PRODUCTNAME, favorite.get_productname());
        values.put(COLUMN_OVERVIEW, favorite.get_overview());
        values.put(COLUMN_RELEASEDATE, favorite.get_release_date());
        values.put(COLUMN_STATUS, favorite.get_status());
        values.put(COLUMN_VOTEAVERAGE, favorite.get_vote_average());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_FAVORITES, null, values);
        //we are done with it
        db.close();
    }


    //Delete a product from a database

    public void deleteProduct(String productName){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_FAVORITES + " WHERE " + COLUMN_PRODUCTNAME + "=\"" + productName + "\";");
    }

    //print out database as a string
    public List getfromDatabase(){
        List item = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_FAVORITES + "";

        //cursor point to a location in your results
        Cursor cursor = db.rawQuery(query,null);
        //move to first row in your results
        cursor.moveToFirst();


        int max = cursor.getCount();
        for(int i = 0;i<max;i++) {
            item.add(cursor.getString(i));
        }

        db.close();

        return item;


    }


    public String getname(String Name){
        List items = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT "+COLUMN_PRODUCTNAME+" FROM " + TABLE_FAVORITES + " WHERE "+COLUMN_PRODUCTNAME+" = '"+Name+"'";

        //cursor point to a location in your results
        Cursor cursor = db.rawQuery(query,null);
        //move to first row in your results
        cursor.moveToFirst();


        int max = cursor.getCount();
        for(int i = 0;i<max;i++) {
            items.add(cursor.getString(i));
        }

        db.close();
        String item = items.toString();
        return item;


    }


}