package com.ansome22.moviesandtvshows.UI;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ansome22.moviesandtvshows.R;
import com.ansome22.moviesandtvshows.adapter.DerpAdapter;
import com.ansome22.moviesandtvshows.model.DerpData;
import com.ansome22.moviesandtvshows.model.ListItem;

import java.util.ArrayList;

public class List extends AppCompatActivity implements DerpAdapter.ItemClickCallback {


    private ArrayList listData;

    private RecyclerView mRecyclerView;
    private DerpAdapter mAdapter;

    ArrayList<String> iconresult  = new ArrayList<String>();
    Bitmap[] icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            Bundle b = intent.getExtras();
            ArrayList<String> titleresult;

            titleresult = b.getStringArrayList("com.ansome22.moviesandtvshows.item");
            String[] title = titleresult.toArray(new String[0]);
            DerpData.setTitles(title);

            ArrayList<String> idresult = b.getStringArrayList("com.ansome22.moviesandtvshows.item.id");
            String[] id = idresult.toArray(new String[0]);
            DerpData.setId(id);


            //icon = (Bitmap[]) b.getParcelableArray("com.ansome22.moviesandtvshows.icon");
            //DerpData.setIcons(icon);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.rec_list);
        // use a linear layout manager
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new DerpAdapter(DerpData.getListData(),this);

        // specify an adapter
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setItemClickCallback(this);

    }

    @Override
    public void onitemClick(int p) {
        listData = (ArrayList) DerpData.getListData();
        ListItem item =  (ListItem) listData.get(p);
        Intent i = new Intent(this, Detail.class);
        i.putExtra("Selected_Item", item.getTitle());
        i.putExtra("Selected_Item_ID", item.getId());

        startActivity(i);
    }
}
