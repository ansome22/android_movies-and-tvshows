package com.ansome22.moviesandtvshows.UI;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.ansome22.moviesandtvshows.MyIntentService;
import com.ansome22.moviesandtvshows.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Genre extends Fragment {
    Spinner spinner;
    Activity activity;
    Snackbar mysnackbar;
    String key = "371162c4c94e3c1bbb369dc0ce79898d";
    ArrayList<String> titleresult;
    ArrayList<String> urlresult;
    ArrayList<String> id;
    public static final String intentkey = "com.ansome22.moviesandtvshows.item";
    public static final String intentkeyid = "com.ansome22.moviesandtvshows.item.id";
    int genreid;
    int genreposition;

    public Genre() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_genre, container, false);

    setSpinnerContent(view);

        Spinner spinner = (Spinner) view.findViewById(R.id.GenreDropdown);
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                        // An item was selected. You can retrieve the selected item using
                        Object itemSelected = parent.getItemAtPosition(position);
                        mysnackbar = Snackbar.make(getActivity().findViewById(R.id.myCoordinatorLayout), "Searching....", Snackbar.LENGTH_LONG);
                        genreposition = position;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        Context context = activity;
                        CharSequence text = "Select A Genre";
                        int duration = Toast.LENGTH_LONG;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                }
        );

        Button searchbtnHit = (Button) view.findViewById(R.id.Genrebtn);

        searchbtnHit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genreid = genreposition;
                    mysnackbar = Snackbar.make(getActivity().findViewById(R.id.myCoordinatorLayout), "Searching....", Snackbar.LENGTH_LONG);
                    genre_search(v, genreid);
            }
        });

        return view;
    }



    private void setSpinnerContent(View view){
        Spinner spinner = (Spinner) view.findViewById(R.id.GenreDropdown);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity,
                R.array.genre_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }


    public void genre_search(View v, int postion) {
        mysnackbar.show();
        //get from search/edit text in main
        int[] genres = getResources().getIntArray(R.array.genre_id_array);
        genreid = genres[postion];
        titleresult  = new ArrayList<>();
        urlresult  = new ArrayList<>();
        id  = new ArrayList<>();
        String url = "https://api.themoviedb.org/3/discover/movie?api_key="+key+"&language=en-US&sort_by=popularity.asc&include_adult=false&include_video=false&page=1&with_genres="+genreid+"";

        MyIntentService.startActionGetList(getContext(),url);
    }

}