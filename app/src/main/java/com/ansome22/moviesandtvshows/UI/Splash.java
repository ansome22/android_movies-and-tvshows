package com.ansome22.moviesandtvshows.UI;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.ansome22.moviesandtvshows.R;

public class Splash extends Activity {
    private int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent splash = new Intent();
                startActivity(new Intent(Splash.this,MainActivity.class));
                finish();
            }
        },SPLASH_TIME_OUT);

    }
}
