package com.ansome22.moviesandtvshows.UI;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.ansome22.moviesandtvshows.MyIntentService;
import com.ansome22.moviesandtvshows.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Search extends Fragment {
    EditText search;
    String Search;
    String adult;
    //key for API
    String key = "371162c4c94e3c1bbb369dc0ce79898d";
    String page;
    Snackbar mysnackbar;
    String searchtext;
    Activity activity;
    ArrayList<String> titleresult;
    ArrayList<String> urlresult;
    ArrayList<String> id;

    public Search() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        }
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        // Inflate the layout for this fragment
        ImageButton btnHit = (ImageButton) view.findViewById(R.id.search_button);
        search = (EditText) view.findViewById(R.id.search_editText);
        //to anoounce to user the earch button worked
        mysnackbar = Snackbar.make(getActivity().findViewById(R.id.myCoordinatorLayout), "Searching....", Snackbar.LENGTH_LONG);

        btnHit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchtext = search.getText().toString();
                if (searchtext.isEmpty()) {
                    mysnackbar = Snackbar.make(getActivity().findViewById(R.id.myCoordinatorLayout), "Please enter something to search", Snackbar.LENGTH_LONG);
                    mysnackbar.show();
                }else {
                    mysnackbar = Snackbar.make(getActivity().findViewById(R.id.myCoordinatorLayout), "Searching....", Snackbar.LENGTH_LONG);
                    button_search(v);
                }
            }
        });




        return view;
    }

    public void button_search(View v) {
        mysnackbar.show();
        //get from search/edittext in main
        Search = search.getText().toString();
        //need to get from settings
        adult = "true";
        page = "1";
        titleresult  = new ArrayList<>();
        urlresult  = new ArrayList<>();
        id  = new ArrayList<>();

        String url = "https://api.themoviedb.org/3/search/multi?api_key="+key+"&language=en-UK&query="+Search+"&page="+page+"&include_adult="+adult+"";

        MyIntentService.startActionGetList(getContext(),url);
    }


}
