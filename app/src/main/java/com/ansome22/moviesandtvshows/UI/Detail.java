package com.ansome22.moviesandtvshows.UI;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.ansome22.moviesandtvshows.MySingleton;
import com.ansome22.moviesandtvshows.R;
import com.ansome22.moviesandtvshows.adapter.MyDBHandler;
import com.ansome22.moviesandtvshows.model.Favorites;

import org.json.JSONException;
import org.json.JSONObject;


public class Detail extends AppCompatActivity {
    String key = "371162c4c94e3c1bbb369dc0ce79898d";
    String itemid;
    String url;
    String budget = "";
    String release_date;
    String title;
    String overview;
    String status;
    String vote_average;
    MyDBHandler dbHandler;
    ImageLoader mimageloader;
    NetworkImageView Poster;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        //to be used to allow user to favorite items
        fab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Adding to Favorites...", Snackbar.LENGTH_LONG).show();
                favorite(view);
            }
        });
        //create database handler for database
        dbHandler = new MyDBHandler(this, null, null, 1);

        //get extra from intent
        Intent intent = getIntent();
        title = intent.getStringExtra("Selected_Item");
        //change activity title to items title
        setTitle(title);
        //get items id
        itemid = intent.getStringExtra("Selected_Item_ID");
        //add paramters to url
        url = "https://api.themoviedb.org/3/movie/" + itemid + "?api_key=" + key + "&language=en-US";
        TextView DetailResults = (TextView) findViewById(R.id.DetailResults);
        //get id for image
        Poster = (NetworkImageView) findViewById(R.id.Poster);

        discover(DetailResults, url);
/*
        if (dbHandler.getname(title) != title) {
            fab.setImageResource(R.mipmap.ic_star_border_black_36dp);
        } else {
            fab.setImageResource(R.mipmap.ic_star_black_36dp);
        }
        */

    }

    public void discover(final TextView DetailResults, String url) {

        RequestQueue queue = Volley.newRequestQueue(this);


        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response: ", response.toString());
                String title2 = response.optString("title");
                String name2 = response.optString("name");

                if (!title2.equals(title) && !name2.equals(title)) {
                    String url = "https://api.themoviedb.org/3/tv/" + itemid + "?api_key=" + key + "&language=en-US";
                    discover(DetailResults, url);
                }

                try {
                    if (!response.optString("budget").isEmpty()) {
                        budget = response.get("budget").toString();
                    }
                    overview = response.get("overview").toString();
                    if (!response.optString("release_date").isEmpty()) {
                        release_date = response.get("release_date").toString();
                    } else {
                        release_date = response.get("first_air_date").toString();
                    }
                    status = response.get("status").toString();
                    vote_average = response.get("vote_average").toString();

                    if (budget == "") {
                        budget = "Unknown";
                    }

                    get_icon(response.getString("poster_path"), Poster);

                    DetailResults.setText("Overview: " + overview +
                            "\n\n" + "Release Date: " + release_date +
                            "\n\n" + "Budget: $" + budget +
                            "\n\n" + "Status: " + status +
                            "\n\n" + "Vote: " + vote_average
                    );


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error: ", error.toString());
            }
        });

// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getBaseContext()).addToRequestQueue(jsObjRequest);
    }


    public void opennetflix(View v) {
        Intent i;
        PackageManager manager = getPackageManager();
        try {
            i = manager.getLaunchIntentForPackage("com.netflix.mediaclient");
            if (i == null)
                throw new PackageManager.NameNotFoundException();
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.netflix.com/"));
            startActivity(intent);
        }

    }

    public void openamazon(View v) {
        Intent i;
        PackageManager manager = getPackageManager();
        try {
            i = manager.getLaunchIntentForPackage("com.amazon.avod.thirdpartyclient");
            if (i == null)
                throw new PackageManager.NameNotFoundException();
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.primevideo.com/"));
            startActivity(intent);
        }

    }

    public void openhulu(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.hulu.com/"));
        startActivity(intent);
    }


    //Add a product to the database
    public void favorite(View view) {
        try {
            if (dbHandler.getname(title) != title) {
                Favorites product = new Favorites(title, overview, release_date, status, vote_average);
                dbHandler.addProduct(product);
                fab.setImageResource(R.mipmap.ic_star_black_36dp);
            } else {
                //to add remove product
                dbHandler.deleteProduct(title);
                fab.setImageResource(R.mipmap.ic_star_border_black_36dp);
            }
        }catch (SQLiteException e){
            e.printStackTrace();
        }
    }


    public void get_icon(final String url, final NetworkImageView poster) {
        mimageloader = MySingleton.getInstance(getBaseContext()).getImageLoader();
        poster.setBackground(null);
        poster.setImageUrl("http://image.tmdb.org/t/p/w342/"+url, mimageloader);

    }
}
