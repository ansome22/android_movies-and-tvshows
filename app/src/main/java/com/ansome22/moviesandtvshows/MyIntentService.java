package com.ansome22.moviesandtvshows;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ansome22.moviesandtvshows.UI.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {
    String key = "371162c4c94e3c1bbb369dc0ce79898d";
    ArrayList<String> titleresult  = new ArrayList<>();
    ArrayList<String> urlresult  = new ArrayList<>();
    ArrayList<String> id  = new ArrayList<>();
    Bitmap[] iconresult;
    public static final String intentkey = "com.ansome22.moviesandtvshows.item";
    public static final String intentkeyid = "com.ansome22.moviesandtvshows.item.id";
    public static final String intentkeyicon = "com.ansome22.moviesandtvshows.item.icon";


    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_GetList = "layout.action.GetList";
    private static final String ACTION_Favorite = "layout.action.Genre";
    private static final String ACTION_GetDetails = "layout.action.GetDetails";

    // TODO: Rename parameters
    private static final String EXTRA_URL = "layout.extra.QUERY";
    private static final String EXTRA_Adult = "layout.extra.Adult";
    private static final String EXTRA_PAGE = "layout.extra.Page";

    public MyIntentService() {
        super("MyIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionGetList(Context context, String URL) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_GetList);
        intent.putExtra(EXTRA_URL, URL);
        context.startService(intent);
    }


    public static void startActionGetDetails(Context context, String URL) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_GetDetails);
        intent.putExtra(EXTRA_URL, URL);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionFavorite(Context context, String Query, String Adult) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_Favorite);
        intent.putExtra(EXTRA_URL, Query);
        intent.putExtra(EXTRA_Adult, Adult);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GetList.equals(action)) {
                final String URL = intent.getStringExtra(EXTRA_URL);
                handleActionGetList(URL);
            } else if (ACTION_Favorite.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_URL);
                final String param2 = intent.getStringExtra(EXTRA_Adult);
                handleActionFavorites(param1, param2);
            } else if (ACTION_GetDetails.equals(action)){
                final String URL = intent.getStringExtra(EXTRA_URL);
                //handleActionGetDetails(URL);
            }
        }
    }

    /**
     * Handle action Search in the provided background thread with the provided
     * parameters.
     */
    private void handleActionGetList(String URL) {
        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest (Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray obj = response.getJSONArray("results");
                    for (int i=0; i<obj.length(); i++) {
                        JSONObject item = obj.getJSONObject(i);
                        //if tv show
                        if(item.has("name")){
                            String test = item.getString("name");
                            titleresult.add(test);
                            //if movie
                        }else{
                            titleresult.add(item.getString("title"));
                        }
                        urlresult.add(item.optString("poster_path"));
                        id.add(item.optString("id"));
                    }

                    //To move to list
                    if(titleresult.size()!=0) {
                        Intent i = new Intent(getApplicationContext(), List.class);
                        i.putExtra(intentkey, titleresult);
                        if(iconresult != null) {
                            i.putExtra(intentkeyicon, iconresult);
                        }
                        i.putExtra(intentkeyid,id);
                        startActivity(i);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error: ", error.toString());
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
    }

    /**
     * Handle action Genre in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFavorites(String param1, String param2) {
        // TODO: Handle action Genre
        throw new UnsupportedOperationException("Not yet implemented");
    }


}
