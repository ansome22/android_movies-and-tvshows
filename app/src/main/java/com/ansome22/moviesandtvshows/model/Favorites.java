package com.ansome22.moviesandtvshows.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ansome22 on 8/05/2017.
 */

public class Favorites {
    /*
    underscore labels that variable as a class variable so it is accessible through every method that belongs to the class,
    Naming conventions like that help make the code easier to follow when the files become bigger and need to be scaled.﻿
     */

    private int _id;
    private String _productname;
    private  String _overview;
    private  String _release_date;
    private  String _status;
    private  String _vote_average;

    public String get_productname() {
        return _productname;
    }

    public String get_overview() {
        return _overview;
    }

    public void set_overview(String _overview) {
        this._overview = _overview;
    }

    public String get_release_date() {
        return _release_date;
    }

    public void set_release_date(String _release_date) {
        this._release_date = _release_date;
    }

    public String get_status() {
        return _status;
    }

    public void set_status(String _status) {
        this._status = _status;
    }

    public String get_vote_average() {
        return _vote_average;
    }

    public void set_vote_average(String _vote_average) {
        this._vote_average = _vote_average;
    }

    public Favorites(){
    }


    public Favorites(String productname,String _overview,String _release_date,String _status,String _vote_average) {
        this._productname = productname;
        this._overview = _overview;
        this._release_date = _release_date;
        this._status = _status;
        this._vote_average = _vote_average;

    }

    //setters allows to give object one of the properties
    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_productname(String _productname) {
        this._productname = _productname;
    }

    //to rectrieve
    public int get_id() {
        return _id;
    }

    public String get_favoritename() {
        return _productname;
    }




}
