package com.ansome22.moviesandtvshows.model;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ansome22 on 6/05/2017.
 */

public class DerpData {
    private static  String[] titles = {"If you see this something went wrong"};

    public static String[] getId() {
        return id;
    }

    public static void setId(String[] id) {
        DerpData.id = id;
    }

    private static  String[] id = {"If you see this something went wrong"};

    private static Bitmap[] icons;

    public static void setTitles(String[] titles) {
        DerpData.titles = titles;
    }

    public static void setIcons(Bitmap[] icons) {
        DerpData.icons = icons;
    }

    public static List<ListItem> getListData() {
        List<ListItem> data = new ArrayList<>();


            for (int i = 0; i < titles.length; i++) {
                ListItem item = new ListItem();
                //item.setImageResid(icons[i]);
                item.setTitle(titles[i]);
                item.setId(id[i]);
                data.add(item);
            }

        return data;
    }


}
