package com.ansome22.moviesandtvshows.model;

import android.graphics.Bitmap;

/**
 * java representation of our data to be displayed in the recycler view
 * Created by ansome22 on 6/05/2017.
 */


public class ListItem {
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;
    private boolean favourite = false;

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
